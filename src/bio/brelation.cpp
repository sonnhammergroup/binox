/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "brelation.h"

BRelation::BRelation(unsigned int size)
{
    m_size      =  size;
    m_random    =  std::vector<int>(size,0);
    m_orig      =  std::vector<int>(size,0);
    m_origPFC   =  std::vector<double>(size,0);
    m_rndMax    =  std::vector<int>(size,0);
}

unsigned int BRelation::size()
{
   return m_size;
}



void BRelation::setRndLink(unsigned int pos, int a)
{
    m_random.at(pos) += a;
}

int BRelation::getRndLink(unsigned int pos)
{
    return m_random.at(pos);
}

int BRelation::sumRndLink()
{
    int sum=0;
    for (unsigned int j = 0; j < m_size; j++)
        sum += m_random.at(j);

    return sum;
}

void BRelation::setRndMax(unsigned int pos, int a)
{
    m_rndMax.at(pos) += a;
}

int BRelation::getRndMax(unsigned int pos)
{
    return m_rndMax.at(pos);
}



void BRelation::setOrigLink(unsigned int pos, int a)
{
    m_orig.at(pos) += a;
}

int BRelation::getOrigLink(unsigned int pos)
{
    return m_orig.at(pos);
}

int BRelation::sumOrigLink()
{
    int sum=0;
    for (unsigned int j = 0; j < m_size; j++)
        sum += m_orig.at(j);

    return sum;
}



void BRelation::setPFC(unsigned int pos, double a)
{
    m_origPFC.at(pos) += a;
}

double BRelation::getPFC(unsigned int pos)
{
    return m_origPFC.at(pos);
}
