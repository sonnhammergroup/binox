/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BBASE_H
#define BBASE_H

#include <set>
#include <string>
#include <vector>

using namespace std;

class BBase {

protected:
    string m_Name;
    unsigned int m_Id;

public:

    BBase(){}

    virtual ~BBase(){}


    unsigned int getId(){
        return m_Id;
    }

    void setID(unsigned int id){
        m_Id = id;
    }

    string getName(){
        return m_Name;
    }

    bool operator <(const BBase& rhs) const
       {    return m_Name < rhs.m_Name;     }

    bool operator ==(const BBase &rhs) const
       {    return m_Name == rhs.m_Name;      }

    bool operator !=(const BBase &rhs) const
       {    return m_Name != rhs.m_Name;      }

    struct cmp{
        bool operator() ( BBase* lhs, BBase* rhs)
        { return lhs->m_Name < rhs->m_Name; }
    };

};
#endif // BBASE_H
