/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#ifndef BINTOOLS_H
#define BINTOOLS_H

#include "bio/bcomposite.h"
#include "graph/gtypes.h"


#include "utils/utypes.h"


bool getGroupRelations(pair<BComposite,BComposite> &composites, set<int> &uniqueGenes, unsigned int min);

bool generateGeneRealtionMatrix(BComposite* Genome,pair<Network,Network> &networks);

bool generateDegreeMap(pair<Network,Network> &networks, map<int, vector<Record> > &degRecordsMap);

bool generateCompositeRelationMatrix(pair<BComposite,BComposite> &groups);

bool generateRandomNetworkFile(BinoXConfig conf, BComposite &genome);

void printNetworkStatistics(int genes, int links);

void printGeneSetStatistics(string nameA, int sizeA, string nameB, int sizeB);

void printConfigurations(string method, int iterations);

void printRandomNetConfig(BinoXConfig config);

void printNetworkINFO(string name, double cuttOff);

#endif // BINTOOLS_H
