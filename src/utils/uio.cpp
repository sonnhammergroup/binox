/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/


#include "uio.h"

#include <fstream>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

//Checks if file exists and returns the input with vectors(row) of vectors(col) of strings(cell)

bool readFile(string path, string sep, vector<vector<string> > &data){

    data.clear();

    vector<string> col;
    string line;
    ifstream file;

    file.open(path.c_str());
    if (!file)
    {
        return false;
    }

    while(getline(file, line))
    {
        boost::split(col, line, boost::is_any_of(sep));
        data.push_back(col);
    }

    return true;
}



bool writeFile( vector<vector<string> > data, fileInfo out){

    string absolutePath;

    if(out.absolutePath!="")
        absolutePath = out.absolutePath + "/" + out.fileName;
    else
        absolutePath = out.fileName;


    int col = data.at(0).size();
    int row = data.size();

    ofstream  file;

    file.open(absolutePath.c_str());
    if (!file)
        return false;

    for (int j = 0; j < row; j++) {

        file << data.at(j).at(0) ;

        for (int i = 1; i < col; i++) {

            file << out.sep << data.at(j).at(i) ;


        }

        file << endl;
    }

    file.close();
    return true;
}

bool checkDir(string path){

   if(!boost::filesystem::is_directory(path))
       return false;

   return true;
}


bool createDir(string path, bool replace){

    if(checkDir(path) && replace)
    {
         boost::filesystem::path path_to_remove(path);
         for (boost::filesystem::directory_iterator end_dir_it, it(path_to_remove); it!=end_dir_it; ++it) {
           boost::filesystem::remove_all(it->path());
         }
         boost::filesystem::remove_all(path);
    }

    if(!checkDir(path))
    {
        boost::filesystem::path dir(path);
        if (!boost::filesystem::create_directory(dir))
            return false;

        return true;
    }

    return false;
}


bool getAbsolutePath(string &path){

    boost::filesystem::path p(path);
    path = p.parent_path().c_str();
    path = boost::filesystem::absolute(path).c_str();
    path = path + "/";

    return true;
}

bool getFileName(string &path){

    boost::filesystem::path p(path);
    path = p.filename().c_str();

    return true;
}


