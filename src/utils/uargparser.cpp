/*************************************************************
 *                   Christoph Ogris                         *
 *             christoph.ogris@scilifelab.se                 *
 *************************************************************/

#include "uargparser.h"
#include "uio.h"

#include <boost/program_options.hpp>
#include <iostream>


using namespace std;
using namespace boost::program_options;


/*************************************************************
 *                    PARSE INPUT VARIABLES                  *
 *************************************************************/
bool uInitArgParser(int argc, char** argv, BinoXConfig &config)
{

   string workingDir = "";
   getAbsolutePath(workingDir);

   try
      {

        // following variable will change after parsing the input variables

        config.newNetwork = false;         // is original network used
        config.preProcessedNetwork = false;    // is a already processed net used
        config.createPreProcessedNetwork = false;      // write randomised network
        config.evaluateGroups = false;      // write randomised network

        //options
        options_description desc("BinoX Options");
        desc.add_options()
                ("help,h", "Produces this message.")
                ("version,v", "Display the version number")
                ("relationType,d", value<string>(&config.process.relationType)->default_value("+-"), "[+-]: compute enrichment scores when number of links is higher than expected by chance, otherwise compute depletion scores\n[+]: compute only enrichment scores. [-]: compute only depletion scores")
                ("network,n", value<string >(&config.fileFormatNetwork.io.absolutePath), "[path]. Filepath to undirected Network File")
                ("randomNetwork,r", value< string > (&config.filePreProcessedNetwork.absolutePath), "[path]. Loads a BinoX random network file.")
                ("cutoff,c", value<double>(&config.process.cutOff)->default_value(0), "Lowest link weight to include in network.")
                ("method,m", value<string>(&config.process.method)->default_value("SecondOrder"),"Method [LinkSwap]: Link Permutation, swap links between nodes.\nMethod [Assignment]: Link Assignment, assign links uniformly randomly, conserve degree.\nMethod [SecondOrder]: Link Assignment + Second-order, same as 1 but attempt to conserve second-order properties also.\nMethod [NodeSwap]: Node Permutation, swap node labels only.")
                ("iter,i", value<unsigned int>(&config.process.iterations)->default_value(150),"[randomizations].Number of network randomizations.")
                ("geneSetA,a", value<string >(&config.fileFormatGroupA.io.absolutePath), "[path]. Filepath to geneSet A")
                ("geneSetB,b", value<string >(&config.fileFormatGroupB.io.absolutePath), "[path]. Filepath to geneSet B")
                ("geneSetST,q", value<string >(&config.fileFormatGroupB.io.absolutePath), "[path]. Filepath to stResultData")
                ("stCutoff,w", value<double>(&config.fileFormatGroupB.stProp.cutOff)->default_value(0), "Lowest gene confidence.")
                ("minGenes,g", value<unsigned int>(&config.process.minSetSize)->default_value(5),"Set the lower bound on the minimum number of genes a group should have to be included in the analysis.")
                ("output,o", value< string > (&config.fileFormatResult.io.absolutePath), "[path]. User specified results file.")
                ("outputFormat,p", value<string>(&config.fileFormatResult.fileFormat)->default_value("compact"),"[compact]: includes only binomial and hypergeometric CDF \n [large]: includes all information necessery to compute CDF\n")
                ("seed,s", value<unsigned int>(&config.process.seed),"Set seed for network randomization")
                ;

        // parse variables
        variables_map vm;
        store(command_line_parser(argc, argv).
                  options(desc).run(), vm);
        notify(vm);

        //  CHECK FOR ORIGINAL NETWORK
        //  parse information to [config]
        if ( vm.count("network"))
        {
            //load absolut file path and name
            config.fileFormatNetwork.io.fileName = config.fileFormatNetwork.io.absolutePath;
            getAbsolutePath(config.fileFormatNetwork.io.absolutePath);
            getFileName(config.fileFormatNetwork.io.fileName);

            config.newNetwork = true;

            //if only new network is given randomize and save it
            if( !vm.count("randomNetwork") && !vm.count("geneSetA") &&  !vm.count("geneSetB") )
            {
                config.filePreProcessedNetwork.fileName = "BinoX-" + config.fileFormatNetwork.io.fileName + ".randNet";
                config.filePreProcessedNetwork.absolutePath = config.filePreProcessedNetwork.absolutePath;   //  Output file location.

                config.createPreProcessedNetwork = true;
            }

            if( vm.count("randomNetwork"))
            {
                config.filePreProcessedNetwork.fileName = config.filePreProcessedNetwork.absolutePath;
                getAbsolutePath(config.filePreProcessedNetwork.absolutePath);
                getFileName(config.filePreProcessedNetwork.fileName);

                config.createPreProcessedNetwork = true;
            }

        }


        //  CHECK FOR RANDOMNETWORK
        if(     vm.count("randomNetwork") &&  !vm.count("network"))
        {
            config.preProcessedNetwork = true;

            config.filePreProcessedNetwork.fileName = config.filePreProcessedNetwork.absolutePath;
            getAbsolutePath(config.filePreProcessedNetwork.absolutePath);
            getFileName(config.filePreProcessedNetwork.fileName);
        }



        //  CHECK FOR GENE SET
        //  parse information to [config]
        if (    vm.count("geneSetA") ||  vm.count("geneSetB"))
        {


            //if groupA is ST file
            if(vm.count("geneSetST")){
                //load absolut file path and name
                config.fileFormatGroupB.isStFile = true;
                config.stAnalysisMode = true;

            }

            if(!vm.count("geneSetST")){
                config.fileFormatGroupB.isStFile = false;
                config.stAnalysisMode = false;

                if( vm.count("geneSetA") &&  !vm.count("geneSetB")){
                    config.fileFormatGroupB.io.absolutePath = config.fileFormatGroupA.io.absolutePath ;
                    config.process.intra = true;
                    }
                if( !vm.count("geneSetA") &&  vm.count("geneSetB")){
                    config.fileFormatGroupA.io.absolutePath = config.fileFormatGroupB.io.absolutePath;
                    config.process.intra = true;
                    }
            }

            //load absolut file path and name
            config.fileFormatGroupA.io.fileName = config.fileFormatGroupA.io.absolutePath;
            getAbsolutePath(config.fileFormatGroupA.io.absolutePath);
            getFileName(config.fileFormatGroupA.io.fileName); 

            //load absolut file path and name
            config.fileFormatGroupB.io.fileName = config.fileFormatGroupB.io.absolutePath;
            getAbsolutePath(config.fileFormatGroupB.io.absolutePath);
            getFileName(config.fileFormatGroupB.io.fileName);



            // GET OUTPUT INFORMATION
            if(vm.count("output"))
            {
                config.fileFormatResult.io.fileName = config.fileFormatResult.io.absolutePath;
                getAbsolutePath(config.fileFormatResult.io.absolutePath);
                getFileName(config.fileFormatResult.io.fileName);
            }

            if(!vm.count("output"))
            {
                config.fileFormatResult.io.fileName = config.fileFormatGroupA.io.absolutePath;
                config.fileFormatResult.io.fileName = config.fileFormatGroupA.io.fileName + "_VS_" + config.fileFormatGroupB.io.fileName;
            }
            config.evaluateGroups = true;
        }

        if (!vm.count("seed"))
        {
            config.process.seed = time(NULL);
        }

        // ERROR HANDLING HELP SECTION
        if (    vm.count("version") )
        {
            cout << "BinoX version:" << config.version << endl ;
            exit(1);
        }


        if (     argc < 2 ||  vm.count("help") )
        {
            cout << "BinoX " << config.version << endl;
            cout << "Option 1 - Networkfile based:\t-n NETWORK_FILE -a GENESET_A_FILE -b GENESET_B_FILE" << endl;
            cout << "Option 2 - Filesystem based:\t-f PATH_TO_CONFIGURATION_FILE -a GENESET_A_FILE -b GENESET_B_FILE" << endl;
            cout << "Option 3 - Create filesystem:\t-n NETWORK_FILE" << endl;
            cout << "Option 4 - use ST Analysis results:-n NETWORK_FILE -a PATHWAY_FILE -st ST_FILE" << endl;
            cout << desc;
            exit(1);
        }

      }

      catch(std::exception& e)
      {
        return false;
      }



    return true;
}
